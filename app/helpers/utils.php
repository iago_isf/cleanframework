<?php
function dump_die($variable)
{
  var_dump($variable);
  die();
}

function urlHelper($url){
  header('Location:'. URLROOT . '/'. $url );
  return;
}

function isLoggedIn(){
  if(isset($_SESSION['user_id'])){
    return true;
  } else {
    return false;
  }
}

function isActive($option){
  switch ($option){
    case 'home':
      if (strpos($_SERVER['REQUEST_URI'], $option)){
        return 'border-bottom border-primary border-2';
      }
      break;
    default:
      return false;
      break;
  }
}
?>