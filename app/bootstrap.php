<?php
require_once 'config/config.php';
require_once 'helpers/utils.php';

spl_autoload_register(function ($className) { // Si el nombre de uno de los archivos de clase no sigue las reglas de nomenclatura se corre el riesgo de que no funcione
  require_once 'libraries/'.$className.'.php';
});
?>